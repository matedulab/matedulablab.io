![icon](https://github.com/rvalla/matedulab/raw/master/assets/img/icon_64.png)

# MatEduLab (testing new materials)

- **Random walk**: a group of dots walks randomly although by clicking on the screen you can change the trend.  
- **Random walk (1d)**: as *Random walk* but in a rectilinear path.  
- **Random melodies**: for listen to melodies that have been made randomly and not so randomly.  
- **Two signla synthesizer**: a synthesizer that, depending on where you make click, adjust the frequency for two
signals.  
- **Rock, paper & scissors**: a game to explore the concepts related to probability with your students.  

Test our materials in the [beta site](https://matedulab.gitlab.io).
