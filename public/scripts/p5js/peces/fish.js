class fish {

	constructor(dna, printdna, x, y, w, lw){		
		this.dna = dna;
		this.printdna = printdna;
		this.ismarked = false;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = w * 0.5;
		this.bw = w * 0.866;
		this.tw = w * 0.185;
		this.th = this.h * 0.866;
		this.lw = lw;
		this.fontsize = this.w * 0.180;
		this.phenotyping(this.dna);
	}

	display() {
		ellipseMode(CENTER);
		let eyex;
		strokeWeight(this.lw);
		stroke(this.c)
		noFill();
		arc(this.x, this.y, this.w, this.w, PI/6,5*PI/6);
		arc(this.x, this.y + this.w/2, this.w, this.w, 7*PI/6,-PI/6);
		if (this.orientation){
			arc(this.x + this.bw, this.y, this.w, this.w, 4*PI/6,5*PI/6);
			arc(this.x + this.bw, this.y + this.w/2, this.w, this.w, 7*PI/6,-4 * PI/6);
			line(this.x + this.bw / 2 + this.tw, this.y + this.h / 7, this.x + this.bw / 2 + this.tw, this.y + this.th);
			eyex = this.x - (this.bw / 3);
		} else {
			arc(this.x - this.bw, this.y, this.w, this.w, PI/6, 2*PI/6);
			arc(this.x - this.bw, this.y + this.w/2, this.w, this.w, -2*PI/6, -PI/6);
			line(this.x - this.bw / 2 - this.tw, this.y + this.h / 7, this.x - this.bw / 2 - this.tw, this.y + this.th);
			eyex = this.x + (this.bw / 3); 
		}
		this.pattern();
		fill(this.c);
		noStroke();
		if (this.printdna) {
			textAlign(LEFT, TOP);
			textSize(this.fontsize);
			text(this.dna, this.x - this.bw * 0.25, this.y + this.h * 0.3);
		}
		if (this.theeye){
			ellipse(eyex, this.y + this.h / 3, this.lw * 2, this.lw * 2);
		} else {
			ellipse(eyex, this.y + this.h / 3, this.lw * 4, this.lw * 2);
		}
		if (this.ismarked){
			fill(this.pc);

			ellipse(this.x, this.y + this.h / 2, this.bw / 4, this.bw / 4);
		}
	}

	contains(x, y) {
		let isIn = false;
		let startx;
		if (this.orientation){
			startx = this.x - (this.bw / 2);
		} else {
			startx = this.x - (this.bw / 2) - this.tw;
		}
		let endx = startx + this.bw + this.tw;
		if (x > startx && x < endx){
			if (y > this.y && y < this.y + this.h) {
				isIn = true;
			}
		}
		return isIn;
	}

	move(x,y){
		this.x = x;
		this.y = y;
	}

	mark(){
		if (this.ismarked){
			this.ismarked = false;
		} else {
			this.ismarked = true;
		}
	}

	pattern(){
		let x1 = this.x - (this.bw * 0.25);
		let y1 = this.y + this.h * 0.11;
		let x2 = x1 + this.bw * 0.5;
		let y2 = y1 + this.h - this.h * 0.22;
		let xstep = (x2 - x1) / 5;
		let ystep = (y2 - y1) / 5;
		let mx = xstep / 2;
		let my = ystep / 2;
		strokeWeight(this.lw);
		stroke(this.pc);
		if (this.p === 1) {
			for (let i = 0; i < 5; i++){
				for (let j = 0; j < 5; j++){
					point(mx + x1 + i * xstep, my + y1 + j * ystep);
				}
			}
		} else if (this.p === 2) {
			for (let i = 0; i < 5; i++){
				line(mx + x1, my + y1 + i * ystep, x2 - mx, my + y1 + i * ystep);
			}
		} else if (this.p === 3) {
			for (let i = 0; i < 2; i++){
				for (let j = 0; j < 4; j++){
					line(mx + x1 + xstep * 2 * i, my + y1 + (j+1) * ystep, mx + x1 + (2*i+1) * xstep, my + y1 + j * ystep);
					line(mx + x1 + (2*i+1) * xstep, my + y1 + j * ystep, mx + x1 + (2*i+2) * xstep, my + y1 + (j+1) * ystep);
				}
			}
		}
	}

	phenotyping(dna){
		let bases = dna.split("");
		if (bases[0] === "A"){
			this.c = color(30,104,144);
			this.pc = color(30,104,144,125);
		} else if (bases[0] === "C"){
			this.c = color(135,22,100);
			this.pc = color(135,22,100,125);
		} else if (bases[0] === "G"){
			this.c = color(190,105,7);
			this.pc = color(190,105,7,125);
		} else if (bases[0] === "T"){
			this.c = color(22,105,22);
			this.pc = color(22,105,22,125);
		}
		if (bases[1] === "C" || bases[1] === "T"){
			this.theeye = true;
		} else {
			this.theeye = true;
		}
		if (bases[2] === "A"){
			this.p = 0;
		} else if (bases[2] === "C"){
			this.p = 1;
		} else if (bases[2] === "G"){
			this.p = 2;
		} else if (bases[2] === "T"){
			this.p = 3;
		}
		if (bases[3] === "C" || bases[3] === "T"){
			this.orientation = false;
		} else {
			this.orientation = true;
		}
	}

}
