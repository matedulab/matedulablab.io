class gamesounds {

  constructor(){
		this.lose = new p5.Oscillator("square");
		this.lose.freq(110);
		this.win = new p5.Oscillator("triangle");
		this.win.freq(440);
		this.water = new p5.Noise("pink");
		this.winEnv = new p5.Envelope(0.05, 0.9, 0.8, 0.1);
		this.winEnv.connect(this.win);
		this.loseEnv = new p5.Envelope(0.1, 0.9, 0.2, 0.1);
		this.loseEnv.connect(this.lose);
		this.waterEnv = new p5.Envelope(0.4, 0.7, 1.3, 0.08);
		this.waterEnv.connect(this.water);
  }

	playWin() {
		this.win.start();
		this.winEnv.play(this.win);
	}

	playLose() {
		this.lose.start();
		this.loseEnv.play(this.lose);
	}

	playWater() {
		this.water.start();
		this.waterEnv.play(this.water);
	}

}
