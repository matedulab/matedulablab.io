let matedulab, banner, logosize, titlesize, counttextsize;
let offsetw, offseth, titleoffset, labeloffset, framew, frameh, lakew, lakeh, panelw, panelh, xcenter, ycenter;
let title, authors, fishinglabel, fishingcount, helpf, helpl;
let state, gsounds, thebutton;
let fontb, font, colors;
let lake, lakegrid, fishes, cells, fishsize, printdna, population, mincatch, maxcatch;

function preload() {
	matedulab = loadImage('p5js/peces/assets/img/matedulab.png');
	banner = loadImage('p5js/peces/assets/img/banner.png');
	font = loadFont('p5js/peces/assets/fonts/Comfortaa_Regular.ttf');
	fontb = loadFont('p5js/peces/assets/fonts/Comfortaa_Bold.ttf');
}

function setup() {
	getAudioContext().suspend();
	createCanvas(windowWidth, windowHeight);
	thecanvas = document.getElementsByTagName("canvas")[0];
	thecanvas.addEventListener("mousedown", processEv, false);
	xcenter = width / 2;
	ycenter = height / 2;
	config = getURLParams();
	startConfig(config);
	displayBanner();
	textAlign(LEFT, TOP);
	textFont(fontb);
	print("MatEduLab: Peces en el lago");
	print("version: 1.00");
}

function getScreen() {
	background(200);
	printTexts();
	image(matedulab, offsetw, offseth / 8, logosize, logosize);
	lakeWater();
	displayLake(fishes, cells);
	thebutton.display(mouseX,mouseY);
}

function processEv(){
	switch (state) {
		case 1:
			if (thebutton.contains(mouseX, mouseY)){
				fishes = throwNet();
				cells = randomList(fishes.length,0,lakegrid[0]*lakegrid[1]);
				fishingcount += 1;
				gsounds.playWater();
				getScreen();
			} else {
				for (let i = 0; i < fishes.length; i++){
					if (lake[fishes[i]].contains(mouseX, mouseY)){
						lake[fishes[i]].mark();
						getScreen();
						break;
					}
				}
			}
			break;
		case 0:
			getScreen();
			state = 1
			break;
		case -1:
			userStartAudio();
			gsounds = new gamesounds();
			state = 0;
			getScreen();
			printHelp();
			break;
	}
}

function throwNet() {
	let n = round(random(mincatch,maxcatch));
	let f = randomList(n, 0, lake.length);
	return f;
}

function displayLake(fishes, cells) {
	for (let i = 0; i < cells.length; i++) {
		position = getFishPosition(cells[i]);
		lake[fishes[i]].move(position[0],position[1]);
		lake[fishes[i]].display();
	}
}

function getFishPosition(cell) {
	row = Math.floor(cell/lakegrid[1]);
	col = cell%lakegrid[1];
	cw = lakew * 0.9 / lakegrid[0];
	ch = lakeh * 0.9 / lakegrid[1];
	position = [];
	if (framew >= frameh) {
		position[0] = offsetw + panelw + fishsize * 0.75 + cw * row;
		position[1] = offseth + titleoffset * 1.5 + ch * col;
	} else {
		position[0] = offsetw + fishsize * 0.75 + cw * row;
		position[1] = offseth + titleoffset * 1.5 + ch * col;
	}
	return position;
}

function randomList(n, min, max) {
	let list = [];
	for (let i = 0; i < n; i++) {
		let d = true;
		while(d) {
			list[i] = (Math.floor(random(min,max)));
			d = duplicates(list);
		}
	}
	return list;
}

function duplicates(list) {
	let d = false;
	for (let i = 0; i < list.length - 1; i++) {
		if (list[i] === list[list.length - 1]){
			d = true;
			print("opa");
			break;
		}
	}
	return d;
}

function lakeWater() {
	noStroke();
	fill(colors[6]);
	if (framew >= frameh) {
		rect(offsetw + panelw, offseth + titleoffset, lakew, lakeh, lakew / 50);
	} else {
		rect(offsetw, offseth  + titleoffset, lakew, lakeh, lakew / 50);
	}
}

function printTexts() {
	noStroke();
	textAlign(LEFT, TOP);
	textSize(titlesize);
	fill(colors[0]);
	text(title, offsetw + logosize, offseth / 3);
	textSize(titlesize/2);
	text(authors, offsetw + logosize, offseth / 3 + titlesize * 1.2);
	fill(colors[2]);
	textSize(counttextsize);
	textAlign(CENTER, CENTER);
	if (framew >= frameh) {
		text(fishinglabel, offsetw + panelw * 0.5, offseth + titleoffset + labeloffset);
		text(fishingcount, offsetw + panelw * 0.5, offseth + titleoffset + labeloffset + counttextsize * 1.2);
	} else {
		text(fishinglabel, offsetw + panelw * 0.25, offseth + titleoffset + labeloffset + lakeh);
		text(fishingcount, offsetw + panelw * 0.75, offseth + titleoffset + labeloffset + lakeh);
	}
}

function printHelp() {
	let d1, d2;
	let c1 = [0,0];
	let c2 = [0,0];
	if (framew > frameh) {
		c1[0] = offsetw + panelw / 2
		c1[1] =	offseth + titleoffset + panelh * 0.80,
		c2[0] = offsetw + panelw + lakew * 0.5;
		c2[1] = offseth + titleoffset + lakeh * 0.5;
		d1 = panelw / 5;
		d2 = lakeh / 3;
	} else {
		c1[0] = offsetw + framew / 2
		c1[1] =	offseth + titleoffset + panelh * 0.66 + lakeh,
		c2[0] = offsetw + panelw * 0.5;
		c2[1] = offseth + titleoffset + lakeh * 0.5;
		d1 = panelh / 2;
		d2 = lakew / 3;
	}
	fill(220, 190);
	stroke(colors[5]);
	strokeWeight(width / 150);
	ellipse(c1[0], c1[1], d1, d1);
	ellipse(c2[0], c2[1], d2, d2);
	noStroke();
	textSize(counttextsize);
	fill(colors[0]);
	textAlign(CENTER, CENTER);
	text(helpf, c1[0], c1[1]);
	text(helpl, c2[0], c2[1]);
}

function startConfig(config) {
	state = -1;
	fishes = [];
	cells = [];
	lakegrid = [];
	fishingcount = 0;
	titlesize = map(width, 300, 1200, 30, 40);
	counttextsize = map(width, 300, 1200, 25, 50);
	colors = [];
	colors[0] = color(180, 5, 125);
	colors[1] = color(250, 5, 180);
	colors[2] = color(22, 125, 180);
	colors[3] = color(36, 180, 255);
	colors[4] = color(250, 200, 5);
	colors[5] = color(255, 240, 5);
	colors[6] = color(160, 190, 205);
	framew = width * 0.9;
	frameh = height * 0.8;
	offsetw = (width - framew)/2;
	offseth = (height - frameh)/2;
	titleoffset = frameh * 0.10;
	logosize = frameh * 0.20;
	if (framew >= frameh) {
		panelw = framew / 4;
		panelh = frameh;
		lakew = framew - panelw;
		lakeh = frameh;
		labeloffset = panelh * 0.15;
		lakegrid[0] = 4;
		lakegrid[1] = 4;
		thebutton = new button(offsetw + panelw / 2, offseth + titleoffset + panelh * 0.80, panelw / 10, colors[0], colors[0], "", colors[2]);
	} else {
		panelw = framew;
		panelh = frameh / 4;
		lakew = framew;
		lakeh = frameh - panelh;
		labeloffset = panelh * 0.20;
		lakegrid[0] = 3;
		lakegrid[1] = 5;
		thebutton = new button(offsetw + framew / 2, offseth + titleoffset + panelh * 0.66 + lakeh, panelh / 5, colors[0], colors[0], "", colors[2]);
	}
	number = Number(config.maxcatch);
	if (typeof(number) === "number" && Number.isInteger(number)) {
		maxcatch = number;
	} else {
		maxcatch = 6;
	}
	number = Number(config.mincatch);
	if (typeof(number) === "number" && Number.isInteger(number)) {
		mincatch = number;
	} else {
		mincatch = 4;
	}
	let string = config.lan;
	if (typeof string === "string" && string === "eng") {
		title = "Fishes in the lake";
		authors = "Chorny, Coll, Milrud, Pezzatti";
		fishinglabel = "Catches:";
		helpf = "fishing";
		helpl = "the lake";
	} else {
		title = "Peces en el lago";
		authors = "Chorny, Coll, Milrud, Pezzatti";
		fishinglabel = "Pescas:";
		helpf = "pescá";
		helpl = "el lago";
	}
	string = config.population;
	if (typeof string === "string") {
		population = string;
	} else {
		population = "ATAA,ACAA,AGAA,AAAC,AAAG,AAAT,ACAC,ACAG,ACAT,AGAC,"
		population += "AGAG,AGAT,ATAC,ATAG,ATAT,CAAA,CAAC,CAAG,CAAT,CCAA,"
		population += "CCAC,CCAT,CTAA,CTAG,CTAC,CTAT,CGAG,GGAA,GAAG,GAAA,"
		population += "AACA,TCAC,GTAT,GAAT,GGAG,AACC,AACG,ACCA,ATCA,TCGA,"
		population += "CACA,GTCT,GTCA,GACA,TCGC,CCCA,GACT,GCCC,GACC,ACGG"
	}
	string = config.printdna;
	if (typeof string === "string" && string === "false") {
		printdna = false;
	} else {
		printdna = true;
	}
	loadLake(population)
}

function loadLake(population){
	lake = [];
	let dnas = population.split(",");
	let weight;
	if (framew > frameh) {
		fishsize = lakew / 6;
	} else {
		fishsize = lakeh / 5;
	}
	weight = fishsize / 30;
	for (let i = 0; i < dnas.length; i++){
		lake.push(new fish(dnas[i],printdna, xcenter,ycenter,fishsize,weight))
	}
}

function displayBanner() {
	if (framew > frameh){
		iw = framew * 0.55;
	} else {
		iw = framew * 0.85;
	}
	ih = iw * 0.6425;
	image(banner, (width - iw) / 2, (height - ih) / 2, iw, ih);
}

function displayRegions() {
	if (framew >= frameh) {
		fill(0);
		rect(offsetw, offseth + titleoffset, panelw, panelh);
		fill(100);
		rect(offsetw + panelw, offseth + titleoffset, lakew, lakeh);
	} else {
		fill(0);
		rect(offsetw, offseth  + titleoffset + lakeh, panelw, panelh);
		fill(100);
		rect(offsetw, offseth  + titleoffset, lakew, lakeh);
	}
}
