let state, bg, shadow, correctc, basec;
let logosize, logox, logoy;
let thebutton, buttonsize, buttonmargin
let componentsize, barlabels, barlabelssize, barlabelsoffset, barlabelsline;
let helplabels;
let barstart, barwidth, barheigth, barmargin, barcolors, barpanel, barbg;
let fq_enabled, am_enabled, ph_enabled, cp_enabled;
let signals, thesignal, thecomponent, activesignal, error;
let signal_1, signal_2;

function preload() {
	matedulab = loadImage('p5js/img/matedulab.png');
	banner = loadImage('p5js/img/curvefitting.png');
	font = loadFont('p5js/fonts/UbuntuMono-R.ttf');
	fontb = loadFont('p5js/fonts/UbuntuMono-B.ttf');
}

function setup() {
  getAudioContext().suspend();
  createCanvas(windowWidth, windowHeight);
  thecanvas = document.getElementsByTagName("canvas")[0];
  thecanvas.addEventListener("mousedown", processEv, false);
  config = getURLParams();
  startConfig(config);
  background(bg);
  displayBanner();
  startSignals();
  state = -2;
}

function getScreen(){
  background(bg);
  //signal_1.freq(map(mouseX, 0, width, 220, 880));
  //signal_2.freq(map(mouseY, 0, height, 220, 880));
  displayBars();
  displayText();
  thebutton.display();
  displayShadows();
  signals[activesignal].display();
  thesignal.display();
  image(matedulab, logox, logoy, logosize, logosize);
}

function processEv() {
 switch (state) {
    case -2:
       // userStartAudio();
       // signal_1.amp(0.4);
       // signal_2.amp(0.4);
       // signal_1.start();
       // signal_2.start();
        state = activesignal;
        setUserSignal(activesignal);
        getScreen();
        printHelp();
        break;
    case -1:
        newChallenge();
        getScreen();
        break;
    case 0:
        if (mouseX > barstart && mouseY < 3 * (barmargin + barheight)){
          clickBar(mouseX, mouseY);
        } else if (mouseX > logox && mouseY > logoy){
          newChallenge();
          getScreen();
        }
        getScreen();
        checkState();
        break;
    case 1:
        if (mouseX > barstart && mouseY < 3 * (barmargin + barheight)){
          clickBar(mouseX, mouseY);
        } else if (mouseX > logox && mouseY > logoy){
          newChallenge();
          getScreen();
        }
        getScreen();
        checkState();
        break;
    case 2:
        if (mouseX > barstart && mouseY < 3 * (barmargin + barheight)){
          clickBar(mouseX, mouseY);
        } else if (mouseX > logox && mouseY > logoy){
          newChallenge();
          getScreen();
        }
        getScreen();
        checkState();
        break;
    default:
        if (thebutton.contains(mouseX, mouseY)){
          thecomponent = (thecomponent + 1)%thesignal.size;
        } else if (mouseX > barstart && mouseY < 3 * (barmargin + barheight)){
          clickBar(mouseX, mouseY);
        } else if (mouseX > logox && mouseY > logoy){
          newChallenge();
          getScreen();
        }
        getScreen();
        checkState();
        break;
    }
}

function keyPressed(){
  if (key === "q" || key === "Q"){
    thesignal.increaseCFrequency(thecomponent);
  } else if (key === "a" || key === "A"){
    thesignal.decreaseCFrequency(thecomponent);
  } else if (key === "w" || key === "W"){
    thesignal.increaseCAmplitude(thecomponent);
  } else if (key === "s" || key === "S"){
    thesignal.decreaseCAmplitude(thecomponent);
  } else if (key === "e" || key === "E"){
    thesignal.increaseCPhase(thecomponent);
  } else if (key === "d" || key === "D"){
    thesignal.decreaseCPhase(thecomponent);
  }
  getScreen();
  checkState();
}

function checkState(){
  if (checkSignal()){
    state = -1;
    thesignal.c = correctc;
    thesignal.display();
  }
}

function checkSignal(){
  return thesignal.isEqual(signals[activesignal], error);
}

function newChallenge(){
  activesignal = (activesignal + 1)%9;
  updateControlBoard(activesignal);
  state = activesignal;
  setUserSignal(activesignal);
}

function updateControlBoard(l){
  if (l === 0){
    fq_enabled = true;
    am_enabled = false;
    ph_enabled = false;
    cp_enabled = false;
  } else if (l === 1){
    fq_enabled = true;
    am_enabled = true;
    ph_enabled = false;
    cp_enabled = false;
  } else if (l === 2){
    fq_enabled = true;
    am_enabled = true;
    ph_enabled = true;
    cp_enabled = false;
  } else {
    fq_enabled = true;
    am_enabled = true;
    ph_enabled = true;
    cp_enabled = true;
  }
}

function displayBars(){
  let fq = thesignal.fq[thecomponent];
  let am = thesignal.am[thecomponent];
  let ph = thesignal.ph[thecomponent];
  noStroke();
  fill(barcolors[0]);
  rect(barstart,barmargin,map(fq,0,6,0,barwidth),barheight);
  fill(barcolors[1]);
  rect(barstart,2 * barmargin + barheight,map(am,0,3,0,barwidth),barheight);
  fill(barcolors[2]);
  rect(barstart,3 * barmargin + 2*barheight,map(ph,0,TWO_PI,0,barwidth),barheight);
}

function displayShadows(){
  noStroke();
  fill(shadow);
  if (!cp_enabled){
    rect(0,0,barstart,4 * barmargin + 3 * barheight);
  }
  if (!fq_enabled){
    rect(barstart,barmargin,barwidth,barheight);
  }
  if (!am_enabled){
    rect(barstart,2 * barmargin + barheight,barwidth,barheight);
  }
  if (!ph_enabled){
    rect(barstart,3 * barmargin + 2 * barheight,barwidth,barheight);
  }
}

function clickBar(x,y){
  if (y < (barmargin + barheight) && fq_enabled){
    let fq = map(x,barstart,width,0,6);
    thesignal.updateCFrequency(thecomponent,fq);  
  } else if (y < 2 * (barmargin + barheight) && am_enabled){
    let am = map(x,barstart,width,0,3);
    thesignal.updateCAmplitude(thecomponent,am);
  } else if (ph_enabled) {
    let ph = map(mouseX,barstart,width,0,TWO_PI);
    thesignal.updateCPhase(thecomponent,ph);
  }
  thesignal.printEquation();
}

function displayText(){
  noStroke();
  fill(barcolors[3]);
  textAlign(LEFT, TOP);
  textSize(componentsize);
  textFont(fontb);
  text(str(thecomponent + 1), buttonsize * 0.9, buttonmargin / 3);
  textSize(barheight);
  text(barlabels[0], barstart, barlabelsoffset);
  text(barlabels[1], barstart, barlabelsoffset + barlabelsline);
  text(barlabels[2], barstart,  barlabelsoffset + barlabelsline * 2);
}

function displayBanner() {
    if (width > height){
        iw = width * 0.5;
    } else {
        iw = height * 0.5;
    }
    ih = iw * 0.54;
    image(banner, (width - iw) / 2, (height - ih) / 2, iw, ih);
}

function printHelp() {
	fill(220, 190);
	stroke(barcolors[2]);
	strokeWeight(width / 150);
  rect(0,0,width,barpanel[1]);
	rect(logox,logoy,logosize,logosize);
	noStroke();
	textSize(componentsize * 1.25);
	fill(bg);
  textAlign(CENTER, CENTER);
	text(helplabels[0], width/2, barpanel[1]/2);
  textSize(componentsize * 0.9);
	text(helplabels[1], logox + logosize/2, logoy + logosize/2);
}


function startConfig(config){
  bg = color(20,50,70);
  shadow = color(20,50,70,160);
  basec = color(255,255,255,150);
  correctc = color(120,255,120,255);
  if (width > height){
    logosize = height/6;
  } else {
    logosize = height/9;
  }
  logox = width - logosize - logosize/4;
  logoy = height - logosize - logosize/4;
  buttonsize = getButtonSize();
  buttonmargin = buttonsize * 1.25;
  barcolors = [color(220,20,150),color(10,150,250),color(220,170,0),color(235)];
  barstart = 2 * buttonsize + buttonmargin / 2;
  barwidth = width - barstart;
  barheight = (buttonmargin + buttonsize * 3)/3 - 5;
  barmargin = 5;
  barpanel = [width, 5 * barmargin + 3 * barheight];
  componentsize = buttonsize * 1.5;
  barlabelssize = barheight;
  barlabelsoffset = barmargin;
  barlabelsline = barlabelssize + barlabelsoffset;
  thecomponent = 0;
  thebutton = new button(buttonmargin, 2 * buttonsize + buttonmargin, buttonsize, barcolors[3], barcolors[3], "", barcolors[3]);
  let string = config.lan;
  if (typeof string === "string" && string === "en") {
    barlabels = ["frequency", "amplitude", "phase"];
    helplabels = ["control board", "new\nsignal"];
  } else {
		barlabels = ["frecuencia", "amplitud", "fase"];
    helplabels = ["tablero de control", "cambiar\nseñal"];
  }
  let number = Number(config.level);
  if (typeof(number) === "number" && Number.isInteger(number)) {
    activesignal = number;
  } else {
   	activesignal = 0;
  }
  number = Number(config.error);
  if (typeof(number) === "number" && number > 0 && number < 1) {
    error = number;
  } else {
    if (width > height){
      error = 0.015;
    } else {
      error = 0.045;
    }
   	
  }
  print(error);
  fq_enabled = true;
  am_enabled = false;
  ph_enabled = false;
  cp_enabled = false;
}

function startSignals(){
  let zero = height/1.6;
  let scale = height/12;
  let step = PI/50;
  signals = [];
  signals.push(new signal(barcolors[0], 1, zero, scale, step, [1], [3], [PI]));
  signals.push(new signal(barcolors[1], 1, zero, scale, step, [1.5], [1.7], [PI]));
  signals.push(new signal(barcolors[2], 1, zero, scale, step, [0.6], [2.1], [PI/2]));
  signals.push(new signal(barcolors[0], 1, zero, scale, step, [0.8,1.6], [2,0.8], [PI,PI]));
  signals.push(new signal(barcolors[1], 1, zero, scale, step, [1.5,3,4.5], [0.7,0.5,0.25], [PI,PI,PI]));
  signals.push(new signal(barcolors[2], 1, zero, scale, step, [2,4,6], [1,0.5,0.25], [PI,PI/2,3*PI/2]));
  signals.push(new signal(barcolors[0], 1, zero, scale, step, [0.5,1,1.5,2], [1,0.5,0.7,0.25], [PI,PI/2,PI/2,0.25]));
  signals.push(new signal(barcolors[1], 1, zero, scale, step, [1,2,3,4,5], [1,0.2,0.7,0.2,0.6], [0,0.5,PI,PI/2,3*PI/2]));
  signals.push(new signal(barcolors[2], 1, zero, scale, step, [0.5,1,2,2.5,3,5], [1,0.5,0.5,0.25,0.25,0.25], [PI,PI,PI,0,0,0]));
  thesignal = new signal(basec, 1, zero, scale, step, [0], [0], [0]);
}

function setUserSignal(s){
  thesignal.c = basec;
  if (s === 0) {
    thesignal.rebuildsignal(1);
    thesignal.updateCAmplitude(0, signals[0].am[0]);
    thesignal.updateCPhase(0, signals[0].ph[0]);
  } else if (s === 1) {
    thesignal.rebuildsignal(1);
    thesignal.updateCPhase(0, signals[1].ph[0]);
  } else if (s === 2) {
    thesignal.rebuildsignal(1);
  } else {
    thesignal.rebuildsignal(signals[s].size);
  }
}

function getButtonSize(){
  let x = width /18;
  let y = height / 24;
  let w = 0;
  if (x < y){
    w = x;
  } else {
    w = y;
  }
  return w;
}