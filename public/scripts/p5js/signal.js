class signal {

  constructor(c, lw, zero, scale, step, fq, am, ph){
    this.size = fq.length;
    this.c = c;
    this.lw = lw;
    this.zero = zero;
    this.scale = scale;
    this.step = step;
    this.fq = fq;
    this.am = am;
    this.ph = ph;
  }

  display(){
    noFill();
    strokeWeight(this.lw);
    stroke(this.c);
    for (let i = 0; i < width; i++) {
        let y = this.getSignalY(i);
        line(i,y,i,height);
    }
  }

  getSignalY(x){
    let y = this.zero;
    for (let i = 0; i < this.size; i++){
        y += Math.sin(this.fq[i]*this.step*x + this.ph[i])*this.am[i]*this.scale;
    }
    return y;
  }

  updateComponent(c, fq, am, ph){
    this.fq[c] = fq;
    this.am[c] = am;
    this.ph[c] = ph;
  }

  updateCFrequency(c, fq){
	  this.fq[c] = fq;
  }

  updateCAmplitude(c, am){
	  this.am[c] = am;
  }

  updateCPhase(c, ph){
	  this.ph[c] = ph;
  }

  increaseCFrequency(c){
    this.fq[c] += 0.01;
  }

  decreaseCFrequency(c){
    this.fq[c] -= 0.01;
  }

  increaseCAmplitude(c){
    this.am[c] += 0.01;
  }

  decreaseCAmplitude(c){
    this.am[c] -= 0.01;
  }

  increaseCPhase(c){
    this.ph[c] += 0.01;
  }

  decreaseCPhase(c){
    this.ph[c] -= 0.01;
  }

  printEquation(){
    print("Términos de la Ecuación:");
    for (let i = 0; i < this.size; i++){
      let m = str(i + 1) + ". " + str(this.am[i].toFixed(2)) + " * sen(" +
              str(this.fq[i].toFixed(2)) + "x + " + str(this.ph[i].toFixed(2)) + ")";
      print(m);
    }
  }

  rebuildsignal(components){
    this.size = components;
    this.fq = [];
    this.am = [];
    this.ph = [];
    for (let i = 0; i < components; i++){
      this.fq[i] = 0;
      this.am[i] = 0;
      this.ph[i] = 0;
    }
  }

  reset(){
    for (let i = 0; i < this.size; i++){
      this.fq[i] = 0;
      this.am[i] = 0;
      this.ph[i] = 0;
    }
  }

  isEqual(othersignal, error){
    let equal = true;
    if (this.size != othersignal.size){
      equal = false;
    } else {
      for (let i = 0; i < this.size; i++){
        if (abs(1 - this.fq[i]/othersignal.fq[i]) > error){
          equal = false;
          break;
        } else if (abs(1 - this.am[i]/othersignal.am[i]) > error * 1.5){
          equal = false;
          break;
        } else if (abs(1 - this.ph[i]/othersignal.ph[i]) > error * 3){
          equal = false;
          break;
        }
      }
    }
    return equal;
  }
}